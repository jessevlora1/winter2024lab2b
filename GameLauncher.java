import java.util.*;
public class GameLauncher{
	public static void main (String[] args){
		
		System.out.println("Hello, please choose 1 to play Hangman or 2 to play Wordle.");
		Scanner reader = new Scanner(System.in);
													//Takes the input number to determine which game
		int gameNum = reader.nextInt();
		
		if(gameNum == 1){							// if int is 1, Hangman runs
		
			System.out.println("Please choose a four lettered word.");
		
			String word = reader.nextLine();
	
			Hangman.runGame(word);
		}
		
		if(gameNum == 2){							// if int is 2, Wordle runs
			
		String randomWord = Wordle.generateWord();
		
		Wordle.runGame(randomWord);
		}
		else{										//if the user did not choose 1 or 2, the program closes
			System.out.println("Since you did not choose 1 or 2, please try again.");
		}
	
	}
	
}