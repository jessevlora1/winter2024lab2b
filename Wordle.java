import java.util.*;
import java.util.Random;

public class Wordle{
	
	
	public static String generateWord(){ 
		String[] wordList = {"shade", "media", "blind", "route", "audio", "pause", "alien", "plane", "fraud", "final", "rhyme", "choke", "vegan", "worth", "curse", "crush", "prate", "taper",  "naive"};
		
		Random random = new Random();
		int wordNum = random.nextInt(wordList.length);
		
		String randomWord = wordList[wordNum];
		
		return randomWord.toUpperCase();		
		
	}
	
	public static boolean letterInWord(String word, char letter){
		for(int i = 0; i < word.length(); i++){
			if(word.charAt(i) == (letter)){
				return true;
			}
			
		}
		
		
		return false;
	}
	
	public static boolean letterInSlot(String word, char letter, int positionGuess){
		for(int i = 0; i < word.length(); i++){
			if(i == positionGuess && word.charAt(i) == (letter))
				return true;												
		}

		return false;
	}
	
	public static String[] guessWord(String targetWord, String userWord){
		
		 String[] coloursOfLetters = new String[5];
		
		for(int i = 0; i < targetWord.length(); i++){
			if(letterInSlot(targetWord, userWord.charAt(i), i) == true){  //if the letter is in the same index as the index of the userWord, turn green
				coloursOfLetters[i] = "green";
			}
			
			else if(letterInWord(targetWord, userWord.charAt(i)) == true){	//if the letter is in the word, but not at the same index, turn yellow
					coloursOfLetters[i] = "yellow";
			}
			else{
				coloursOfLetters[i] = "white"; // at this point, it means the letter was nowhere in the word
			}
		}
		
		return coloursOfLetters;
	}
	
	public static void presentResults(String userWord, String[] colours){
		final String ANSI_GREEN = "\u001b[32m";
		final String ANSI_YELLOW = "\u001b[33m";
		final String ANSI_RESET = "\u001b[0m";
		
		String toPrint = "";
		
		for(int i = 0; i < colours.length; i++){
			if(colours[i] == "green"){
			
			toPrint += ANSI_GREEN + userWord.charAt(i) + ANSI_RESET;  
			}
			if(colours[i] == "yellow"){
			
			toPrint += ANSI_YELLOW + userWord.charAt(i) + ANSI_RESET;
			}
			if(colours[i] == "white"){
			
			toPrint += ANSI_RESET + userWord.charAt(i)	;
			}
		}
		 
		System.out.println(toPrint);
		
		
		
		
	}
	
	public static String readGuess(){
			Scanner userWord = new Scanner(System.in);
			System.out.println("Type a 5 lettered word to guess.");
			String userGuessWord = userWord.nextLine();
		
		while(userGuessWord.length() < 5){
			
			System.out.println("Please try again");
			
			Scanner newUserWord = new Scanner(System.in);
			System.out.println("Type a word to guess.");
			userGuessWord = newUserWord.nextLine();
			if(userGuessWord.length() == 5){
			break;
			}
		}
		
		String capUserGuessWord = userGuessWord.toUpperCase();
		// System ...
		return capUserGuessWord;
	}
	
	public static void runGame(String targetWord){
		int numOfChances = 0;
		
		while(numOfChances < 6){	
			String userWord = readGuess();
			String[] coloursOfLetters = guessWord(targetWord, userWord);
			
			presentResults(userWord, coloursOfLetters);
			// presentResults is void for now, wordResult doesnt make sense, but its meant to be the printed word in presentResults aka the same word, but in colors
			
			numOfChances += 1;
			
			
			
			
		if(targetWord.equals(userWord)){
			break;
		}
			
		}
		if(numOfChances <=6){
			System.out.println("Congrats, you won!!");
		}
		else{
			System.out.println("you lose!!");
		}
		
	}
	
	
	
			
	
			

}

	
